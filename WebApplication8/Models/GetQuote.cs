﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace WebApplication8.Models
{
    public class Client
    {
           public String FirstName { get; set; }
           public String LastName { get; set; }
           public String Initials { get; set; }
           public String ID_number { get; set; }
           public String CellNumber {get; set;}
           public String CreditCheckDate { get; set; }
           public String CreditScore { get;set; }
           public String DateOfBirth { get; set; }
           public String Email { get; set; }
           public String  Gender { get;set; }
           public String  HomePhone { get;set; }
           public String JudgementAmount { get;set; }
           public String LapseScore { get;set; }
	       public String MaritalStatus { get;set; }
           public String NoOfClaims { get;set; }
           public String NoOfJudgements { get;set; }
           public String PrevInsuranceYears { get;set; }
           public String RetiredYN { get;set; }
           public String TiaIdNo { get;set; }
           public String Title { get;set; }
           public String WorkPhone { get;set; }
           public Address address { get; set; }
           public Vehicle vehicles { get; set; }
           public Item items { get; set; }
           public String PropertySituation { get; set; }
    }
    public class Address
    {
           public String AddressIdNo { get; set;}
           public String AreaCode { get; set; }
           public String CountryCode {get; set;}
           public String Line1 { get; set; }
           public String Line2 { get ; set; }
           public String Line3 { get ; set; }
           public String Line4 { get ; set; }
           public String PostCode { get ; set; }
           public String SuburbName { get ; set; }
           
    }
    public class Vehicle
    {
            public String Colour { get ; set; }
            public String Make { get ; set; }
            public String MmCode { get ; set; }
            public String Model { get ; set; }
            public String NatisNumber { get ; set; }
            public String RegdrvIdNo { get ; set; }
            public String RegdrvName { get ; set; }
            public String RegdrvRelationship { get ; set; }
            public String RegdrvMaritalStatus { get ; set; }
            public String AgreedOrRetail { get ; set; }
            public String RegistrationNumber { get ; set; }
            public String RegularDriverSelfYN { get ; set; }
            public String VehicleStatus { get ; set; }
            public String VehicleType { get ; set; }
            public String VehicleTypeDesc { get ; set; }
            public String VinNumber { get ; set; }
            public String Year { get ; set; }
            public String LicenseCode { get ; set; }
            public String LicenseYear { get ; set; }
            public String AccessoriesValue { get ; set; }
            public String AccessoriesDesc { get ; set; }
            public String AccessoriesYn { get ; set; }
            public String TotalSumInsured { get ; set; }
            public String CoverType { get; set; }
            public String NoOfClaimsMotor { get; set; }

    }
    public class Item
    {
            public String AnnualPremium { get ; set; }
            public String ItemTypeCode { get ; set; }
            public String MonthlyPremium { get ; set; }
            public String ObjectNo { get ; set; }
            public String SumInsured { get ; set; }
            public String SumInsuredMotor { get; set; }
            public String SumInsuredContents { get; set; }
            public String SumInsuredPersonalBelogings { get; set; }
            public String ItemTypeCodeMotor { get; set; }
            public String ItemTypeCodeContents { get; set; }
            public String ItemTypeCodePersonalBelongings { get; set; }
            public String ItemCategory { get; set; }
            public String NumberOfClaimsContent { get; set; }
            public String SumInsuredAllRisk { get; set; }
            public String ItemTypeAllRiskModel { get; set; }
            public String allRiskCategory { get; set; }
    }
    public class Loss
    {
        public String type { get; set; }
        public String amount { get; set; }
        public String year { get; set; }
        public String insurer { get; set; }
        public String policy { get; set; }
    }
}


