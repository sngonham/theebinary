﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using Oracle.ManagedDataAccess.Client;
using System.Web.Http;
using System.Web.Http.Controllers;
using WebApplication8.App_Start;
using WebApplication8.Models;

namespace WebApplication8.Controllers
{
    public class GetQuoteController : ApiController
    {
        [HttpPost]
        public String Index(Client model)
        {
            String results = "";
            OracleConnection Conn = SQLDataConnect.getConnection();
            var actionContext = new HttpActionContext();
            try
            {
                //calling Tia quick quote
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = Conn;
                Conn.Open();
                 cmd.CommandText = "submit_quick_quote";
                //cmd.CommandText = "binaries";
                cmd.CommandType = CommandType.StoredProcedure;

                OracleParameter p_sa_id_number = new OracleParameter();
                p_sa_id_number.ParameterName = "l_sa_id_number";
                p_sa_id_number.OracleDbType = OracleDbType.Varchar2;
                p_sa_id_number.Value = model.ID_number;


                OracleParameter p_retired_yn = new OracleParameter();
                p_retired_yn.ParameterName = "l_retired_yn";
                p_retired_yn.OracleDbType = OracleDbType.Varchar2;
                p_retired_yn.Value = model.RetiredYN;


                OracleParameter p_email = new OracleParameter();
                p_email.ParameterName = "l_email";
                p_email.OracleDbType = OracleDbType.Varchar2;
                p_email.Value = model.Email;

                OracleParameter p_title = new OracleParameter();
                p_title.ParameterName = "l_title";
                p_title.OracleDbType = OracleDbType.Varchar2;
                p_title.Value = model.Title;

                OracleParameter p_first_name = new OracleParameter();
                p_first_name.ParameterName = "l_first_name";
                p_first_name.OracleDbType = OracleDbType.Varchar2;
                p_first_name.Value = model.FirstName;

                OracleParameter p_initials = new OracleParameter();
                p_initials.ParameterName = "l_initials";
                p_initials.OracleDbType = OracleDbType.Varchar2;
                p_initials.Value = model.Initials;

                OracleParameter p_surname = new OracleParameter();
                p_surname.ParameterName = "l_surname";
                p_surname.OracleDbType = OracleDbType.Varchar2;
                p_surname.Value = model.LastName;

                OracleParameter p_date_of_birth = new OracleParameter();
                p_date_of_birth.ParameterName = "l_date_of_birth";
                p_date_of_birth.OracleDbType = OracleDbType.Varchar2;
                p_date_of_birth.Value = model.DateOfBirth;

                OracleParameter p_marital_status = new OracleParameter();
                p_marital_status.ParameterName = "l_marital_status";
                p_marital_status.OracleDbType = OracleDbType.Varchar2;
                p_marital_status.Value = model.MaritalStatus;

                OracleParameter p_gender = new OracleParameter();
                p_gender.ParameterName = "l_gender";
                p_gender.OracleDbType = OracleDbType.Varchar2;
                p_gender.Value = model.Gender;

                OracleParameter p_prev_insurance_years = new OracleParameter();
                p_prev_insurance_years.ParameterName = "l_prev_insurance_years";
                p_prev_insurance_years.OracleDbType = OracleDbType.Varchar2;
                p_prev_insurance_years.Value = model.PrevInsuranceYears;

                OracleParameter p_cell_phone = new OracleParameter();
                p_cell_phone.ParameterName = "l_cell_phone";
                p_cell_phone.OracleDbType = OracleDbType.Varchar2;
                p_cell_phone.Value = model.CellNumber;


                OracleParameter p_home_phone = new OracleParameter();
                p_home_phone.ParameterName = "l_home_phone";
                p_home_phone.OracleDbType = OracleDbType.Varchar2;
                p_home_phone.Value = model.HomePhone;

                OracleParameter p_work_phone = new OracleParameter();
                p_work_phone.ParameterName = "l_work_phone";
                p_work_phone.OracleDbType = OracleDbType.Varchar2;
                p_work_phone.Value = model.WorkPhone;
                                
                OracleParameter p_tia_id_no = new OracleParameter();
                p_tia_id_no.ParameterName = "l_tia_id_no";
                p_tia_id_no.OracleDbType = OracleDbType.Varchar2;
                p_tia_id_no.Value = null;


                OracleParameter p_address_line1 = new OracleParameter();
                p_address_line1.ParameterName = "l_address_line1";
                p_address_line1.OracleDbType = OracleDbType.Varchar2;
                p_address_line1.Value = model.address.Line1;

                OracleParameter p_address_line2 = new OracleParameter();
                p_address_line2.ParameterName = "l_address_line2";
                p_address_line2.OracleDbType = OracleDbType.Varchar2;
                p_address_line2.Value = model.address.Line2; 

                OracleParameter p_address_line3 = new OracleParameter();
                p_address_line3.ParameterName = "l_address_line3";
                p_address_line3.OracleDbType = OracleDbType.Varchar2;
                p_address_line3.Value = model.address.Line3;

                OracleParameter p_address_line4 = new OracleParameter();
                p_address_line4.ParameterName = "l_address_line4";
                p_address_line4.OracleDbType = OracleDbType.Varchar2;
                p_address_line4.Value = model.address.Line4;

                OracleParameter p_address_suburb_name = new OracleParameter();
                p_address_suburb_name.ParameterName = "l_address_suburb_name";
                p_address_suburb_name.OracleDbType = OracleDbType.Varchar2;
                p_address_suburb_name.Value = model.address.SuburbName;

                OracleParameter p_address_post_code = new OracleParameter();
                p_address_post_code.ParameterName = "l_address_post_code";
                p_address_post_code.OracleDbType = OracleDbType.Varchar2;
                p_address_post_code.Value = model.address.PostCode;

                OracleParameter p_address_country_code = new OracleParameter();
                p_address_country_code.ParameterName = "l_address_country_code";
                p_address_country_code.OracleDbType = OracleDbType.Varchar2;
                p_address_country_code.Value = model.address.CountryCode;

                OracleParameter p_address_property_situation = new OracleParameter();
                p_address_property_situation.ParameterName = "l_address_property_situation";
                p_address_property_situation.OracleDbType = OracleDbType.Varchar2;
                p_address_property_situation.Value = model.PropertySituation;

                OracleParameter p_address_address_id_no = new OracleParameter();
                p_address_address_id_no.ParameterName = "l_address_address_id_no";
                p_address_address_id_no.OracleDbType = OracleDbType.Varchar2;
                p_address_address_id_no.Value = null;

                OracleParameter p_credit_score = new OracleParameter();
                p_credit_score.ParameterName = "l_credit_score";
                p_credit_score.OracleDbType = OracleDbType.Varchar2;
                p_credit_score.Value = model.CreditScore;

                OracleParameter p_lapse_score = new OracleParameter();
                p_lapse_score.ParameterName = "l_lapse_score";
                p_lapse_score.OracleDbType = OracleDbType.Varchar2;
                p_lapse_score.Value =model.LapseScore;

                OracleParameter p_no_of_judgements = new OracleParameter();
                p_no_of_judgements.ParameterName = "l_no_of_judgements";
                p_no_of_judgements.OracleDbType = OracleDbType.Varchar2;
                p_no_of_judgements.Value = model.NoOfJudgements;

                OracleParameter p_judgement_amount = new OracleParameter();
                p_judgement_amount.ParameterName = "l_judgement_amount";
                p_judgement_amount.OracleDbType = OracleDbType.Varchar2;
                p_judgement_amount.Value = model.JudgementAmount;

                OracleParameter p_no_of_claims = new OracleParameter();
                p_no_of_claims.ParameterName = "l_no_of_claims";
                p_no_of_claims.OracleDbType = OracleDbType.Varchar2;
                p_no_of_claims.Value = model.NoOfClaims;

                OracleParameter p_credit_check_date = new OracleParameter();
                p_credit_check_date.ParameterName = "l_credit_check_date";
                p_credit_check_date.OracleDbType = OracleDbType.Varchar2;
                p_credit_check_date.Value = model.CreditCheckDate;

                OracleParameter p_registration_number = new OracleParameter();
                p_registration_number.ParameterName = "l_registration_number";
                p_registration_number.OracleDbType = OracleDbType.Varchar2;
                p_registration_number.Value = model.vehicles.RegistrationNumber;

                OracleParameter p_cover_type = new OracleParameter();
                p_cover_type.ParameterName = "l_cover_type";
                p_cover_type.OracleDbType = OracleDbType.Varchar2;
                p_cover_type.Value = model.vehicles.CoverType;

                OracleParameter p_make = new OracleParameter();
                p_make.ParameterName = "l_make";
                p_make.OracleDbType = OracleDbType.Varchar2;
                p_make.Value = model.vehicles.Make;

                OracleParameter p_model = new OracleParameter();
                p_model.ParameterName = "l_model";
                p_model.OracleDbType = OracleDbType.Varchar2;
                p_model.Value = model.vehicles.Model;

                OracleParameter p_mm_code = new OracleParameter();
                p_mm_code.ParameterName = "l_mm_code";
                p_mm_code.OracleDbType = OracleDbType.Varchar2;
                p_mm_code.Value = model.vehicles.MmCode;

                OracleParameter p_year = new OracleParameter();
                p_year.ParameterName = "l_year";
                p_year.OracleDbType = OracleDbType.Varchar2;
                p_year.Value = model.vehicles.LicenseYear;

                OracleParameter p_regular_driver_self_yn = new OracleParameter();
                p_regular_driver_self_yn.ParameterName = "l_regular_driver_self_yn";
                p_regular_driver_self_yn.OracleDbType = OracleDbType.Varchar2;
                p_regular_driver_self_yn.Value = model.vehicles.RegularDriverSelfYN;

                OracleParameter p_regdrv_name = new OracleParameter();
                p_regdrv_name.ParameterName = "l_regdrv_name";
                p_regdrv_name.OracleDbType = OracleDbType.Varchar2;
                p_regdrv_name.Value = model.vehicles.RegdrvName;

                OracleParameter p_regdrv_relationship = new OracleParameter();
                p_regdrv_relationship.ParameterName = "l_regdrv_relationship";
                p_regdrv_relationship.OracleDbType = OracleDbType.Varchar2;
                p_regdrv_relationship.Value = model.vehicles.RegdrvRelationship;

                OracleParameter p_regdrv_id_no = new OracleParameter();
                p_regdrv_id_no.ParameterName = "l_regdrv_id_no";
                p_regdrv_id_no.OracleDbType = OracleDbType.Varchar2;
                p_regdrv_id_no.Value = model.ID_number;

                OracleParameter p_regdrv_marital_status = new OracleParameter();
                p_regdrv_marital_status.ParameterName = "l_regdrv_marital_status";
                p_regdrv_marital_status.OracleDbType = OracleDbType.Varchar2;
                p_regdrv_marital_status.Value = model.MaritalStatus;

                OracleParameter p_vin_number = new OracleParameter();
                p_vin_number.ParameterName = "l_vin_number";
                p_vin_number.OracleDbType = OracleDbType.Varchar2;
                p_vin_number.Value = model.vehicles.VinNumber;

                OracleParameter p_natis_number = new OracleParameter();
                p_natis_number.ParameterName = "l_natis_number";
                p_natis_number.OracleDbType = OracleDbType.Varchar2;
                p_natis_number.Value = model.vehicles.NatisNumber;

                OracleParameter p_vehicle_status = new OracleParameter();
                p_vehicle_status.ParameterName = "l_vehicle_status";
                p_vehicle_status.OracleDbType = OracleDbType.Varchar2;
                p_vehicle_status.Value = model.vehicles.VehicleStatus ;

                OracleParameter p_vehicle_type = new OracleParameter();
                p_vehicle_type.ParameterName = "l_vehicle_type";
                p_vehicle_type.OracleDbType = OracleDbType.Varchar2;
                p_vehicle_type.Value = model.vehicles.VehicleType;

                OracleParameter p_colour = new OracleParameter();
                p_colour.ParameterName = "l_colour";
                p_colour.OracleDbType = OracleDbType.Varchar2;
                p_colour.Value = model.vehicles.Colour;

                OracleParameter p_license_year = new OracleParameter();
                p_license_year.ParameterName = "l_license_year";
                p_license_year.OracleDbType = OracleDbType.Varchar2;
                p_license_year.Value = model.vehicles.LicenseYear;

                OracleParameter p_license_code = new OracleParameter();
                p_license_code.ParameterName = "l_license_code";
                p_license_code.OracleDbType = OracleDbType.Varchar2;
                p_license_code.Value = model.vehicles.LicenseCode;

                OracleParameter p_car_hire_yn = new OracleParameter();
                p_car_hire_yn.ParameterName = "l_car_hire_yn";
                p_car_hire_yn.OracleDbType = OracleDbType.Varchar2;
                p_car_hire_yn.Value ="N";

                OracleParameter p_agreed_or_retail = new OracleParameter();
                p_agreed_or_retail.ParameterName = "l_agreed_or_retail";
                p_agreed_or_retail.OracleDbType = OracleDbType.Varchar2;
                p_agreed_or_retail.Value = model.vehicles.AgreedOrRetail;

                OracleParameter p_accessories_value = new OracleParameter();
                p_accessories_value.ParameterName = "l_accessories_value";
                p_accessories_value.OracleDbType = OracleDbType.Varchar2;
                p_accessories_value.Value = model.vehicles.AccessoriesValue;

                OracleParameter p_accessories_desc = new OracleParameter();
                p_accessories_desc.ParameterName = "l_accessories_desc";
                p_accessories_desc.OracleDbType = OracleDbType.Varchar2;
                p_accessories_desc.Value =model.vehicles;

                

     

                OracleParameter p_suminsured_motor = new OracleParameter();
                p_suminsured_motor.ParameterName = "l_suminsured_motor";
                p_suminsured_motor.OracleDbType = OracleDbType.Varchar2;
                p_suminsured_motor.Value = model.items.SumInsuredMotor;

                OracleParameter p_item_type_code_motor = new OracleParameter();
                p_item_type_code_motor.ParameterName = "l_item_type_code_motor";
                p_item_type_code_motor.OracleDbType = OracleDbType.Varchar2;
                p_item_type_code_motor.Value = model.items.ItemTypeCodeMotor;

                OracleParameter p_suminsured_contents = new OracleParameter();
                p_suminsured_contents.ParameterName = "l_suminsured_contents ";
                p_suminsured_contents.OracleDbType = OracleDbType.Varchar2;
                p_suminsured_contents.Value = model.items.SumInsuredContents;

                OracleParameter p_item_type_code_contents = new OracleParameter();
                p_item_type_code_contents.ParameterName = "l_item_type_code_contents ";
                p_item_type_code_contents.OracleDbType = OracleDbType.Varchar2;
                p_item_type_code_contents.Value = model.items.ItemTypeCodeContents;

                OracleParameter p_suminsured_all_risk = new OracleParameter();
                p_suminsured_all_risk.ParameterName = "l_suminsured_all_risk";
                p_suminsured_all_risk.OracleDbType = OracleDbType.Varchar2;
                p_suminsured_all_risk.Value = model.items.SumInsuredAllRisk;

                OracleParameter p_item_type_code_all_risk = new OracleParameter();
                p_item_type_code_all_risk.ParameterName = "l_item_type_code_all_risk";
                p_item_type_code_all_risk.OracleDbType = OracleDbType.Varchar2;
                p_item_type_code_all_risk.Value = model.items.ItemTypeAllRiskModel; 

                OracleParameter p_all_risk_category = new OracleParameter();
                p_all_risk_category.ParameterName = "l_all_risk_category";
                p_all_risk_category.OracleDbType = OracleDbType.Varchar2;
                p_all_risk_category.Value = model.items.ItemCategory; 

                cmd.Parameters.Add(p_sa_id_number);
                cmd.Parameters.Add(p_retired_yn);
                cmd.Parameters.Add(p_email);
                cmd.Parameters.Add(p_title);
                cmd.Parameters.Add(p_first_name);
                cmd.Parameters.Add(p_initials);
                cmd.Parameters.Add(p_surname);
                cmd.Parameters.Add(p_date_of_birth);
                cmd.Parameters.Add(p_marital_status);
                cmd.Parameters.Add(p_gender);
                cmd.Parameters.Add(p_prev_insurance_years);
                cmd.Parameters.Add(p_cell_phone);
                cmd.Parameters.Add(p_home_phone);
                cmd.Parameters.Add(p_work_phone);
                cmd.Parameters.Add(p_tia_id_no);
                cmd.Parameters.Add(p_address_line1);
                cmd.Parameters.Add(p_address_line2);
                cmd.Parameters.Add(p_address_line3);
                cmd.Parameters.Add(p_address_line4);
                cmd.Parameters.Add(p_address_suburb_name);
                cmd.Parameters.Add(p_address_post_code);
                cmd.Parameters.Add(p_address_country_code);
                cmd.Parameters.Add(p_address_property_situation);
                cmd.Parameters.Add(p_address_address_id_no);
                cmd.Parameters.Add(p_credit_score);
                cmd.Parameters.Add(p_lapse_score);
                cmd.Parameters.Add(p_no_of_judgements);
                cmd.Parameters.Add(p_judgement_amount);
                cmd.Parameters.Add(p_no_of_claims);
                cmd.Parameters.Add(p_credit_check_date);
                cmd.Parameters.Add(p_registration_number);
                cmd.Parameters.Add(p_cover_type);
                cmd.Parameters.Add(p_make);
                cmd.Parameters.Add(p_model);
                cmd.Parameters.Add(p_mm_code);
                cmd.Parameters.Add(p_year);
                cmd.Parameters.Add(p_regular_driver_self_yn);
                cmd.Parameters.Add(p_regdrv_name);
                cmd.Parameters.Add(p_regdrv_relationship);
                cmd.Parameters.Add(p_regdrv_id_no);
                cmd.Parameters.Add(p_regdrv_marital_status);
                cmd.Parameters.Add(p_vin_number);
                cmd.Parameters.Add(p_natis_number);
                cmd.Parameters.Add(p_vehicle_status);
                cmd.Parameters.Add(p_vehicle_type);
                cmd.Parameters.Add(p_colour);
                cmd.Parameters.Add(p_license_year);
                cmd.Parameters.Add(p_license_code);
                cmd.Parameters.Add(p_car_hire_yn);
                cmd.Parameters.Add(p_agreed_or_retail);
                cmd.Parameters.Add(p_accessories_value);
                cmd.Parameters.Add(p_accessories_desc);
                cmd.Parameters.Add(p_suminsured_motor);
                cmd.Parameters.Add(p_item_type_code_motor);
                cmd.Parameters.Add(p_suminsured_contents);
                cmd.Parameters.Add(p_item_type_code_contents);
                cmd.Parameters.Add(p_suminsured_all_risk);
                cmd.Parameters.Add(p_item_type_code_all_risk);
                cmd.Parameters.Add(p_all_risk_category);
                OracleParameter OParameter1 = new OracleParameter();
                OracleParameter OParameter2 = new OracleParameter();

                OParameter1.ParameterName = "l_policy_no";
                OParameter1.OracleDbType = OracleDbType.Varchar2;
                OParameter1.Size = 2000;
                OParameter1.Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add(OParameter1);

                OParameter2.ParameterName = "l_policy_premium";
                OParameter2.OracleDbType = OracleDbType.Varchar2;
                OParameter2.Size = 2000;
                OParameter2.Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add(OParameter2);

                cmd.ExecuteNonQuery();
                results = "output "+cmd.Parameters["l_policy_no"].Value.ToString()+" Monthly Premium- "+ cmd.Parameters["l_policy_premium"].Value.ToString();
                Conn.Close();

                return results;
            }
            catch (Exception err)
            {
                Conn.Close();
                results = err.Message;

                return results;
            }
            finally {
                Conn.Close();
            }

        }
    }
}
