﻿


$(function () {

	var user = localStorage.getItem("user");

	if (user == "" || user == undefined || user == "undefined") {
		openPage("register");
	}
	else {
		openPage("landing");
		$("#hello").html("<h3>Hello, " + user.forename1 + " " + user.surname + "</h3>");	
	}
});


function getUserData() {

	var initials = $("#initials").val();
	var surname = $("#surname").val();
	var Idnumber = $("#idNum").val();



	$.ajax({
		'data': "",
		'contentType': 'application/json; charset=UTF-8',
		'type': 'get',
		'async': true,
		//'dataType': 'json',  
		url: "http://zamfdappv105.mufed.co.za:8015/synapse/external/transunion/rest/consumer/"+Idnumber+"/"+surname+"/"+initials,
		success: function (response) {

			if (response.surname == null || response.surname == "") {
				showMessageCard("w3-red", "Information not found, please make sure you entered correct details.", "");
			}
			else {
				var firstname = response.forename1;
				var lastname = response.surname;
				$("#helloLabel").html("<h3>Hello, " + firstname + " " + lastname + "</h3>");
				openPage("landing");
				localStorage.setItem("user", JSON.stringify(response));

				var userSignup = { Initials: initials, IdNo: Idnumber, surname: surname }
				localStorage.setItem("userSignup", JSON.stringify(userSignup));
			}		

			console.log(response);
		},
		error: function (response) {
			//console.log(response.responseText);
			if (response.responseText == "Not Found") {
				showMessageCard("w3-red", "Information not found, please make sure you entered correct details.", "");
			}
			else {
				showMessageCard("w3-red", "Internal error", "");
			}
		}
	});
}


function openPage(pageName) {
	var i;
	var x = document.getElementsByClassName("page");
	for (i = 0; i < x.length; i++) {
		x[i].style.display = "none";
	}
	document.getElementById(pageName).style.display = "block";
}

function showMessageCard(vClass, vMessage, vTopPosition) {

	$("#messageCard").css("display", "block");

	if (vClass != "") {
		$("#messageCard").addClass(vClass);
	}

	if (vTopPosition != "") {
		$("#messageCard").css("top", vTopPosition);
	}

	$("#messageCard p").html(vMessage);

	setTimeout(function () { hideMessageCard(); }, 7000);

}

function hideMessageCard() {
	$("#messageCard").css("display", "none");
}