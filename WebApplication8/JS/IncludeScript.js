﻿$(function () {
	
    $("#register").load("PAGES/signup.html?v=1.0.1");
    $("#landing").load("PAGES/landing.html?v=1.0.2");
    $("#quotetype").load("PAGES/getQuoteType.html?v=1.0.0");
    $("#quotevehicle").load("PAGES/getQuoteVehicle.html?v=1.0.0");
    $("#quoteitem").load("PAGES/getQuoteItem.html?v=1.0.0");
    $("#quotecontent").load("PAGES/getQuoteContent.html?v=1.0.0");
    $("#claimtrack").load("PAGES/claimTrack.html?v=1.0.0");
    $("#claimtrackresult").load("PAGES/claimTrackResult.html?v=1.0.0");
    $("#preinspection").load("PAGES/Preinspection.html?v=1.0.0");
});