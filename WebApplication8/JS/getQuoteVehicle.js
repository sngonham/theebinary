﻿function getVehicleData() {

	var regNo = $("#VehicleRegNumber").val();

	motorSearchDisable();

	$.ajax({
		'data': "",
		'contentType': 'application/json; charset=UTF-8',
		'type': 'get',
		'async': true,
		//'dataType': 'json',  
		url: "http://zamfdappv105.mufed.co.za:8015/synapse/external/transunion/rest/vehicle/" + regNo,
		success: function (response) {

			if (response.vehicleMake == null || response.vehicleMake == "") {
				showMessageCard("w3-red", "vechicle information not found, please make sure you entered correct registration number.", "");
				motorSearchEnable();
			}
			else {
				showMessageCard("w3-blue", "vechicle information is found you may continue with the quote", "");
				localStorage.setItem("vehicle", JSON.stringify(response));
				showVehicleFields();
			}

			console.log(response);
		},
		error: function (response) {
			motorSearchEnable();
			if (response.responseText == "Not Found") {
				showMessageCard("w3-red", "vechicle information not found, please make sure you entered correct registration number.", "");
			}
			else {
				showMessageCard("w3-red", "Internal error", "");
			}
		}
	});
}


function hideVehicleFields() {
	$("#motorFields").slideUp();
	$("#motorDoneButton").css("visibility", "hidden");
}
function showVehicleFields() {
	$("#motorFields").slideDown();
	$("#motorDoneButton").css("visibility", "visible");
	$("#motorSeatchButton").css("display", "none");
}

function motorSearchDisable() {
	$("#motorSeatchButton").html("Searching...");
	$("#motorSeatchButton").prop("disabled", true);
}

function motorSearchEnable() {
	$("#motorSeatchButton").prop("disabled", false);
	$("#motorSeatchButton").html("Search");
}
