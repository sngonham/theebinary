﻿
var user = JSON.parse(localStorage.getItem("user"));
var userSignup = JSON.parse(localStorage.getItem("userSignup"));
var vehicle = JSON.parse(localStorage.getItem("vehicle"));
var getQuoteModel = "";

function initializeQuote() {

    getQuoteModel = {

        "ID_number": "",

        "FirstName": user.forename1,

        "LastName": user.surname,

        "Initials": userSignup.Initials,

        "CellNumber": user.cellNumber,

        "CreditCheckDate": 0,

        "CreditScore": user.creditScore,

        "DateOfBirth": user.dateOfBirth,

        "Email": user.dateOfBirth,

        "Gender": user.gender,

        "HomePhone": user.homeTelephoneNumber,

        "JudgementAmount": user.judgementsAmount,

        "LapseScore": user.lapseScore,

        "MaritalStatus": user.maritalStatus.description,

        "NoOfClaims": 12.0,  // user entry

        "NoOfJudgements": user.noOfJudgements,

        "PrevInsuranceYears": 14, // user entry

        "RetiredYN": true, // user entry

        "Title": user.title,

        "WorkPhone": user.workTelephoneNumber,

        "address": {

            "AreaCode": user.addressList[0].province.code,

            "CountryCode": "ZAR",

            "Line1": user.addressList[0].line1,

            "Line2": user.addressList[0].line2,

            "Line3": user.addressList[0].province.description,

            "Line4": "N/a", // defaulted

            "PostCode": user.addressList[0].postalCode,

            "SuburbName": user.addressList[0].suburb

        },

        "vehicles": {

            "Colour": vehicle.colourCode,

            "Make": vehicle.vehicleMake,

            "MmCode": vehicle.mmCode,

            "Model": vehicle.modelCode,

            "NatisNumber": "#", // defaulted entry

            "RegdrvIdNo": user.identityNumber1, // defaulted to the current user

            "RegdrvName": user.forename1,

            "RegdrvRelationship": "self", // defaulted to the current user

            "RegdrvMaritalStatus": user.maritalStatus.description,

            "AgreedOrRetail": "Y", // defaulted entry to retail price

            "RegistrationNumber": $("#VehicleRegNumber").val(),

            "RegularDriverSelfYN": "Yes", // defaulted to the current user

            "VehicleStatus": $("#VehicleStatus").val(),

            "VehicleType": vehicle.vehicleTypeCode,

            "VehicleTypeDesc": vehicle.vehicleType, 

            "VinNumber": vehicle.vehicleVinNumber,

            "Year": vehicle.vehicleYear,

            "LicenseCode": $("#LicenseCode").val(),

            "LicenseYear": $("#LicenseYear").val(), 

            "AccessoriesValue": 0, // defaulted

            "AccessoriesDesc": "", // defaulted 

            "AccessoriesYn": "N", // defaulted

            "TotalSumInsured": vehicle.retailPrice, // defaulted to retail price
            "NumberOfClaimsMotor": $("#NumberOfClaimsMotor").val(),
            "CoverType": $("#CoverTypeMotor").val()
        },

        "items": {

            "ItemTypeCodeMotor": "007",
            "ItemTypeCodeContents": "002",
            "ItemTypeCodePersonalBelongings": "004",

            "SumInsuredMotor": vehicle.retailPrice,
            "SumInsuredContents": $("#SumInsuredContent").val(),
            "SumInsuredPersonalBelogings": $("#SumInsuredItem").val(), 
            "ItemCategory": $("#ItemCategory").val(),
            "NumberOfClaimsContents": $("#NumberOfClaimsContents").val() // user entry
        }
    }

}

function saveQuote(quoteType) {
    initializeQuote();
    var quoteArray = localStorage.getItem("quotebucket");
    localStorage.setItem("quotebucket", quoteArray + "," + quoteType);
    localStorage.setItem("quoteload", getQuoteModel);
    openPage('quotetype');
    showMessageCard("w3-blue","Quote Save in bucket list", "");
}
