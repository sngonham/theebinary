﻿function requestClaimInfor() {

	var claimNumber = $("#ClaimNumber").val();

	$.ajax({
		'data': "",
		'contentType': 'application/json; charset=UTF-8',
		'type': 'get',
		'async': true,
		//'dataType': 'json',  
		url: "http://zamfdappv105.mufed.co.za:8016/synapse/lob/claim/rest/claim/" + claimNumber +"/header",
		success: function (response) {
			console.log(response.claimNumber);
			if (response.claimNumber == "" || response.claimNumber == null) {
				showMessageCard("w3-red", "claim number cannot be found..", "");
			}
			else {
				var table = "<table>";
				table += "<tr><td>Claim Date:<td><td></td></tr";
				table += "<tr><td>Lost Description:<td><td></td></tr>";
				table += "<tr><td><td><td></td></tr>";
				table += "<tr><td><td><td></td></tr>";
				table += "</table>";

				var progress = "<ul class='progress-bar-dots'>";
				progress += "<li> <pre>Claim Registration</pre> <br/><pre>" + response.notificationDate +"</pre></li>";
				progress += "<li class='stop'><pre>Assessment</pre></li>";
				progress += "<li> <pre>Payment Processed</pre></li>";
				progress += "</ul>";

				$("#claimdetail").html("<u>Claim details</u><br/><br/><br/>" + progress);
				openPage('claimtrackresult');
			}
		},
		error: function (response) {
			console.log(response);
			if (response.responseText == "Not Found") {
				showMessageCard("w3-red", "claim number cannot be found..", "");
			}
			else {
				showMessageCard("w3-red", "internal error..", "");
			}
			
		}
	});

  

}